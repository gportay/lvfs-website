#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use,protected-access

import json
from typing import Optional, Dict

import requests

from lvfs.firmware.models import Firmware
from lvfs.components.models import Component, ComponentIssue
from lvfs.pluginloader import PluginBase, PluginError, PluginSetting, PluginSettingBool
from lvfs import db


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "nist_nvd")
        self.order_after = ["vince"]
        self.name = "NIST NVD"
        self.summary = "The U.S. government repository of vulnerability data"
        self.settings.append(PluginSettingBool(key="nist_nvd_enable", name="Enabled"))

    def _set_description_for_cve_text(self, issue: ComponentIssue, r_text: str) -> None:

        # parse JSON
        try:
            data = json.loads(r_text)
        except json.decoder.JSONDecodeError as e:
            raise PluginError("Failed to query: {}".format(r_text)) from e
        try:
            cve = data["result"]["CVE_Items"][0]["cve"]
            issue.description = cve["description"]["description_data"][0]["value"]
        except KeyError:
            pass

    def _set_description_for_cve(self, issue: ComponentIssue) -> None:

        # public search
        api = "https://services.nvd.nist.gov/rest/json/cve/1.0/{}".format(issue.value)
        print("requesting {}…".format(api))
        r = requests.get(api, stream=True, timeout=10)
        if r.status_code == 200:
            self._set_description_for_cve_text(issue, r.text)

    def archive_presign(self, fw: Firmware) -> None:

        # add description for CVEs
        for md in fw.mds:
            for issue in md.issues:
                if issue.kind == "cve" and not issue.description:
                    self._set_description_for_cve(issue)


# run with PYTHONPATH=. ./env/bin/python3 plugins/nist_nvd/__init__.py
if __name__ == "__main__":
    plugin = Plugin()
    _fw = Firmware()
    _md = Component()
    _md.issues.append(ComponentIssue(kind="cve", value="CVE-2020-0545"))
    _md.issues.append(ComponentIssue(kind="cve", value="CVE-2020-8696"))
    _fw.mds.append(_md)
    plugin.archive_presign(_fw)
    for _issue in _md.issues:
        print("issue {} = {}".format(_issue.value, _issue.description))
