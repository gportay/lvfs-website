"""

Revision ID: 57ff8594b076
Revises: 910d0804cd4d
Create Date: 2021-04-17 20:52:07.257549

"""

# revision identifiers, used by Alembic.
revision = "57ff8594b076"
down_revision = "305ce4dd953d"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column(
        "component_shard_attributes", sa.Column("comment", sa.Text(), nullable=True)
    )


def downgrade():
    op.drop_column("component_shard_attributes", "comment")
