"""

Revision ID: c8ea83754673
Revises: ad7fdc1f8011
Create Date: 2021-06-22 14:22:43.272709

"""

# revision identifiers, used by Alembic.
revision = "c8ea83754673"
down_revision = "ad7fdc1f8011"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column(
        "component_issues", sa.Column("description", sa.Text(), nullable=True)
    )


def downgrade():
    op.drop_column("component_issues", "description")
