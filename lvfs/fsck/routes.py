#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison

import json
import time
import datetime
from typing import Dict, Any

from flask import (
    Blueprint,
    Response,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import login_required

from lvfs import db, auth

from lvfs.util import admin_login_required, _error_internal
from lvfs.firmware.models import Firmware
from lvfs.fsck.models import Fsck
from lvfs.firmware.utils import _async_sign_fw, _async_upload_ipfs
from lvfs.vendors.models import Vendor
from lvfs.vendors.utils import _async_update_attrs_vendors
from lvfs.metadata.models import Remote
from lvfs.analytics.utils import _async_generate_stats_all

from .utils import (
    _async_fsck,
    _async_fsck_update_descriptions,
    _async_fsck_eventlog_delete,
)

bp_fsck = Blueprint("fsck", __name__, template_folder="templates")


@bp_fsck.route("/")
@login_required
@admin_login_required
def route_view():
    return render_template("fsck.html", category="admin")


@bp_fsck.route("/list")
@login_required
@admin_login_required
def route_list():
    fscks = db.session.query(Fsck).order_by(Fsck.started_ts.desc()).all()
    return render_template("fsck-list.html", category="admin", fscks=fscks)


@bp_fsck.route("/delete/<int:fsck_id>", methods=["POST"])
@login_required
@admin_login_required
def route_delete(fsck_id):

    # get tests
    fsck = db.session.query(Fsck).filter(Fsck.fsck_id == fsck_id).first()
    if not fsck:
        flash("No check matched", "warning")
        return redirect(url_for("fsck.route_list"))
    db.session.delete(fsck)
    db.session.commit()

    flash("File system check results have been deleted", "info")
    return redirect(url_for("fsck.route_list"))


@bp_fsck.route("/<int:fsck_id>/cancel", methods=["POST"])
@login_required
@admin_login_required
def route_cancel(fsck_id):

    # get tests
    fsck = db.session.query(Fsck).filter(Fsck.fsck_id == fsck_id).first()
    if not fsck:
        flash("No check matched", "warning")
        return redirect(url_for("fsck.route_list"))
    fsck.ended_ts = datetime.datetime.utcnow()
    db.session.commit()

    flash("File system check has been cancelled", "info")
    return redirect(url_for("fsck.route_list"))


@bp_fsck.route("/update_descriptions", methods=["POST"])
@login_required
@admin_login_required
def route_update_descriptions():

    for key in ["search", "replace"]:
        if key not in request.form or not request.form[key]:
            return _error_internal("No %s specified!" % key)

    # asynchronously rebuilt
    flash("Updating update descriptions", "info")
    _async_fsck_update_descriptions.apply_async(
        args=(
            request.form["search"],
            request.form["replace"],
        ),
        queue="metadata",
    )

    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/eventlog/delete", methods=["POST"])
@login_required
@admin_login_required
def route_eventlog_delete():

    for key in ["value"]:
        if key not in request.form or not request.form[key]:
            return _error_internal("No %s specified!" % key)

    # asynchronously rebuilt
    flash("Deleting eventlog entries", "info")
    _async_fsck_eventlog_delete.apply_async(
        args=(request.form["value"],),
    )

    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/generate_stats", methods=["POST"])
@login_required
@admin_login_required
def route_generate_stats():

    # asynchronously rebuilt
    flash("Generating stats", "info")
    _async_generate_stats_all.apply_async()
    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/upload_ipfs", methods=["POST"])
@login_required
@admin_login_required
def route_upload_ipfs():

    # asynchronously rebuild
    flash("Uploading IPFS content", "info")
    _async_upload_ipfs.apply_async()
    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/healthcheck")
@auth.login_required
def route_healthcheck():
    """this is unauthenticated!!"""

    # worst case scenario
    item: Dict[str, Any] = {}
    item["DatabaseRead"] = False
    item["DatabaseWrite"] = False
    item["FirmwareSigning"] = False
    item["MetadataSigning"] = False

    try:
        # find the first private firmware uploaded by the admin
        stmt = (
            db.session.query(Vendor.vendor_id)
            .filter(Vendor.group_id == "admin")
            .subquery()
        )
        fw = (
            db.session.query(Firmware)
            .join(stmt, Firmware.vendor_id == stmt.c.vendor_id)
            .join(Remote)
            .filter(Remote.name == "private")
            .order_by(Firmware.firmware_id.asc())
            .limit(1)
            .one()
        )
        item["DatabaseRead"] = True

        # mark firmware unsigned
        fw.signed_timestamp = None
        fw.mark_dirty()
        db.session.commit()
        item["DatabaseWrite"] = True

        # sign file
        _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")
        for _ in range(9):
            time.sleep(1)
            db.session.expire_all()
            if fw.signed_timestamp:
                break
        if fw.signed_timestamp:
            item["FirmwareSigning"] = True

        # check metadata was signed
        if fw.remote.is_dirty:
            item["MetadataSigning"] = True

    except Exception as e:  # pylint: disable=broad-except
        item["Exception"] = str(e)

    dat = json.dumps(item, indent=4, separators=(",", ": "))
    return Response(response=dat, status=200, mimetype="application/json")


@bp_fsck.route("/fs", methods=["POST"])
@login_required
@admin_login_required
def route_fs():

    # asynchronously check
    extended = "extended" in request.form
    if extended:
        flash("Checking full FS consistency as background task…", "info")
    else:
        flash("Checking file system consistency as background task…", "info")
    _async_fsck.apply_async(args=(extended,), queue="metadata")
    _async_update_attrs_vendors.apply_async(queue="metadata")
    return redirect(url_for("fsck.route_list"))
