#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,protected-access

import datetime
from typing import Optional

from sqlalchemy import Column, Integer, Text, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from lvfs import db


class FsckAttribute(db.Model):
    __tablename__ = "fsck_attributes"

    fsck_attribute_id = Column(Integer, primary_key=True)
    fsck_id = Column(Integer, ForeignKey("fscks.fsck_id"), nullable=False, index=True)
    title = Column(Text, nullable=False)
    message = Column(Text, default=None)
    success = Column(Boolean, default=False)

    fsck = relationship("Fsck", back_populates="attributes")

    def __repr__(self) -> str:
        return "FsckAttribute object %s=%s" % (self.title, self.message)


class Fsck(db.Model):

    __tablename__ = "fscks"

    fsck_id = Column(Integer, primary_key=True)
    container_id = Column(Text, default=None)
    started_ts = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    ended_ts = Column(DateTime, default=None)
    extended = Column(Boolean, default=False)

    attributes = relationship(
        "FsckAttribute",
        order_by="asc(FsckAttribute.fsck_attribute_id)",
        lazy="joined",
        back_populates="fsck",
        cascade="all,delete,delete-orphan",
    )

    def add_pass(self, title: str, message: Optional[str] = None) -> None:
        self.attributes.append(
            FsckAttribute(title=title, message=message, success=True)
        )

    def add_fail(self, title: str, message: Optional[str] = None) -> None:
        self.attributes.append(
            FsckAttribute(title=title, message=message, success=False)
        )

    @property
    def is_running(self) -> bool:
        if self.started_ts and not self.ended_ts:
            return True
        return False

    @property
    def color(self) -> str:
        if self.is_running:
            return "info"
        if self.success:
            return "success"
        return "danger"

    @property
    def success(self) -> bool:
        if not self.attributes:
            return True
        for attr in self.attributes:
            if not attr.success:
                return False
        return True

    def __repr__(self) -> str:
        return "Fsck object %s(%s)" % (self.fsck_id, self.success)
