#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison,protected-access

import os
import datetime
from typing import List, Optional, Dict

from sqlalchemy import or_

from cabarchive import CabArchive, CorruptionError
from jcat import JcatFile, JcatBlobKind

from lvfs import db, tq, ploader
from lvfs.util import _get_settings, _get_sanitized_basename, _sanitize_keyword
from lvfs.firmware.utils import _async_sign_fw
from lvfs.components.models import (
    Component,
    ComponentIssue,
    ComponentKeyword,
    ComponentShard,
    ComponentShardChecksum,
    ComponentTranslation,
    ComponentTranslationKind,
)
from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.fsck.models import Fsck
from lvfs.main.models import Event
from lvfs.reports.models import Report, REPORT_ATTR_MAP
from lvfs.queries.models import YaraQuery
from lvfs.users.models import User, UserCertificate
from lvfs.users.utils import (
    _user_disable_notify,
    _user_disable_actual,
    _user_add_message_survey,
)


def _fsck_update_descriptions(search: str, replace: str) -> None:

    for md in db.session.query(Component):
        for tx in md.release_descriptions:
            if not tx.value:
                continue
            if tx.value.find(search) != -1:
                tx.value = tx.value.replace(search, replace)
    db.session.commit()


@tq.task(max_retries=3, default_retry_delay=5, task_time_limit=600)
def _async_fsck_update_descriptions(search: str, replace: str) -> None:
    _fsck_update_descriptions(search, replace)


@tq.task(max_retries=3, default_retry_delay=5, task_time_limit=600)
def _async_fsck_eventlog_delete(value: str) -> None:

    for evt in (
        db.session.query(Event).filter(Event.message.contains(value)).limit(10000)
    ):
        db.session.delete(evt)
    db.session.commit()


def _fsck_firmware_check_exists(fsck: Fsck, fw: Firmware) -> None:
    """revision no longer exists"""
    for rev in fw.revisions:
        if not os.path.exists(rev.absolute_path):
            fsck.add_fail(
                "EFS",
                "Firmware #{} has missing revision {} {}".format(
                    fw.firmware_id, rev.firmware_revision_id, rev.absolute_path
                ),
            )


def _fsck_firmware_consistency(fsck: Fsck, fw: Firmware) -> None:
    """multiple Firmware objects pointing at the same filesystem object"""
    if not fw.revisions:
        return
    for fw2 in (
        db.session.query(Firmware)
        .filter(Firmware.firmware_id != fw.firmware_id)
        .join(FirmwareRevision)
        .filter(
            or_(
                FirmwareRevision.filename == fw.revisions[0].filename,
                Firmware.checksum_upload_sha1 == fw.checksum_upload_sha1,
            )
        )
        .limit(10)
        .all()
    ):
        fsck.add_fail(
            "Database::Firmware",
            "Firmware {} points to {} [SHA1:{}]".format(
                fw2.firmware_id,
                fw2.filename,
                fw2.checksum_upload_sha1,
            ),
        )


def _fsck_issue_upgrade(fsck: Fsck, issue: ComponentIssue) -> None:
    """upgrade issues to latest schema version"""
    if not issue.user:
        issue.user = issue.md.fw.user
        fsck.add_pass(
            "Database::ComponentIssue",
            "Fixed up component issue {} user".format(issue.component_issue_id),
        )
    if not issue.timestamp:
        issue.timestamp = issue.md.fw.timestamp
        fsck.add_pass(
            "Database::ComponentIssue",
            "Fixed up component issue {} timestamp".format(issue.component_issue_id),
        )


def _fsck_issue_value_fixup(fsck: Fsck, issue: ComponentIssue) -> None:
    """fix issues to have correct value format"""
    if issue.value != issue.value.upper():
        issue.value = issue.value.upper()
        fsck.add_pass(
            "Database::ComponentIssue",
            "Fixed up component issue {} with {}".format(
                issue.component_issue_id, issue.value
            ),
        )


def _fsck_keyword_value_fixup(fsck: Fsck, kw: ComponentKeyword) -> None:
    """fix keywords to be valid"""
    new_value = _sanitize_keyword(kw.value)
    if kw.value != new_value:
        kw.value = new_value
        fsck.add_pass(
            "Database::ComponentKeyword",
            "Fixed up component keyword {} with {}".format(kw.component_id, kw.value),
        )


def _fsck_firmware_unsigned(fsck: Fsck) -> None:
    """invalidate the signature of firmware signed with an older key"""

    # unset the signed timestamp as required
    settings = _get_settings()
    signed_epoch = int(settings["signed_epoch"])
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .filter(Firmware.signed_timestamp != None)
        .order_by(Firmware.firmware_id.asc())
    ):
        if fsck.ended_ts:
            return
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
        # we signed this recently enough to be epoch 1
        if fw.signed_timestamp.replace(tzinfo=None) > datetime.datetime(2020, 3, 5):
            fw.signed_epoch = 1
        # not good enough
        if fw.signed_epoch != signed_epoch:
            fw.signed_timestamp = None
            fsck.add_pass(
                "Database::Firmware",
                "Invaliding signing checksum of {}".format(fw.firmware_id),
            )
    db.session.commit()


def _fsck_firmware_metainfo_nonnull(fsck: Fsck, fw: Firmware, arc: CabArchive) -> None:
    """NUL byte in metainfo file"""

    requires_resign: bool = False

    for fn in arc:
        if not fn.endswith(".xml"):
            continue
        if arc[fn].buf[-1] == 0:
            fsck.add_fail(
                "JCat::Firmware", "NUL found in metainfo #{}".format(fw.firmware_id)
            )
            requires_resign = True
            break

    # invalidate and resign
    if requires_resign:
        fw.signed_timestamp = None
        db.session.commit()
        _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")


def _fsck_firmware_pkcs7_cert_valid(fsck: Fsck, fw: Firmware, arc: CabArchive) -> None:
    """missing cert from PKCS#7 cert"""

    requires_resign: bool = False

    # check each signature has a server CERTIFICATE, not just a signature
    try:
        jcat_file = JcatFile(arc["firmware.jcat"].buf)
    except KeyError:
        fsck.add_fail(
            "JCat::Firmware", "No firmware.jcat in archive #{}".format(fw.firmware_id)
        )
        requires_resign = True
    else:
        for md in fw.mds:

            # files that used to be valid, but are no longer allowed
            if not md.release_installed_size:
                continue

            # check all the things that are supposed be signed in the jcat file
            for fn in [md.filename_contents, md.filename_xml]:
                if not fn:
                    continue
                jcat_item = jcat_file.get_item(fn)
                if jcat_item:
                    # is this big enough to include the certificate?
                    jcat_blob = jcat_item.get_blob_by_kind(JcatBlobKind.PKCS7)
                    if not jcat_blob:
                        fsck.add_fail(
                            "JCat::PKCS7",
                            "No cert for #{}".format(fw.firmware_id),
                        )
                        requires_resign = True
                    elif not jcat_blob.data:
                        fsck.add_fail(
                            "JCat::PKCS7",
                            "No valid cert for #{}".format(fw.firmware_id),
                        )
                        requires_resign = True
                    elif len(jcat_blob.data) < 0x400:
                        fsck.add_fail(
                            "JCat::PKCS7",
                            "Invalid cert for #{}".format(fw.firmware_id),
                        )
                        requires_resign = True
                else:
                    fsck.add_fail(
                        "JCat::Component", "No jcat item for #{}".format(fw.firmware_id)
                    )
                    requires_resign = True

    # invalidate and resign
    if requires_resign:
        fw.signed_timestamp = None
        db.session.commit()
        _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")


def _fsck_firmware_check_archive(fsck: Fsck, fw: Firmware) -> None:
    """load archive from disk"""

    # we've done all these already
    if fw.firmware_id < 7000:
        return

    # load cabarchive
    try:
        arc = CabArchive(fw.blob, flattern=True)
    except CorruptionError:
        fsck.add_fail(
            "EFS::CabArchive", "Cannot load firmware archive #{}".format(fw.firmware_id)
        )
        return

    # cert valid
    _fsck_firmware_metainfo_nonnull(fsck, fw, arc)
    _fsck_firmware_pkcs7_cert_valid(fsck, fw, arc)


def _fsck_firmware_resign(fsck: Fsck, fw: Firmware) -> None:
    """unsigned firmware"""
    if fw.signed_timestamp:
        return
    _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")
    fsck.add_pass("Database::Firmware", "Resigning #{}".format(fw.firmware_id))


def _check_component_issues(fw: Firmware) -> bool:
    for md in fw.mds:
        for issue in md.issues:
            if not issue.description:
                return False
    return True


def _fsck_firmware_cve_descriptions(fsck: Fsck, fw: Firmware) -> None:
    """unsigned firmware"""

    # we've done all these already
    if fw.signed_timestamp and fw.signed_timestamp.replace(
        tzinfo=None
    ) > datetime.datetime(2021, 7, 10):
        return

    if _check_component_issues(fw):
        return
    ploader.archive_presign(fw)
    if _check_component_issues(fw):
        fsck.add_pass(
            "Database::ComponentIssue",
            "Setting issue description on #{}".format(fw.firmware_id),
        )


def _fsck_firmware_revisions(fsck: Fsck, fw: Firmware) -> None:
    """add the existing filename to a revision"""
    if fw.revisions:
        return
    fw.revisions.append(FirmwareRevision(filename=fw._filename))
    fsck.add_pass(
        "Database::Firmware", "Adding revision for #{}".format(fw.firmware_id)
    )


def _fsck_firmware_revision_basename(fsck: Fsck, fw: Firmware) -> None:
    """ensure all the firmware revisions filenames are safe"""
    if not fw.signed_timestamp:
        return
    for rev in fw.revisions:
        if rev.filename != _get_sanitized_basename(rev.filename):
            fsck.add_pass(
                "Database::Firmware",
                "Sanitizing basename for #{}".format(fw.firmware_id),
            )
            fw.signed_timestamp = None
            db.session.commit()
            _async_sign_fw.apply_async(args=(fw.firmware_id,), queue="firmware")
            return


def _fsck_component_translations(fsck: Fsck, md: Component) -> None:

    # both map to actual None for the default...
    for translation in md.translations:
        if translation.locale in ["None", "en_US"]:
            translation.locale = None
            fsck.add_fail(
                "Database::Translations",
                "Repaired translation locale from firmware #{}".format(
                    md.fw.firmware_id
                ),
            )

    # look for dups
    locales: Dict[str, bool] = {}
    to_remove: List[ComponentTranslation] = []
    for translation in md.translations:
        key = "{}:{}".format(translation.kind, translation.locale)
        if key in locales:
            to_remove.append(translation)
        else:
            locales[key] = True

    # actually remove the dups
    for translation in to_remove:
        fsck.add_fail(
            "Database::Translations",
            "Removing duplicate {} translation locale from firmware #{}".format(
                translation.locale, md.fw.firmware_id
            ),
        )
        md.translations.remove(translation)


def _fsck_component_release_description(fsck: Fsck, md: Component) -> None:

    if md.release_descriptions:
        return
    if not md.unused_release_description:
        return
    md.translations.append(
        ComponentTranslation(
            kind=ComponentTranslationKind.RELEASE_DESCRIPTION,
            value=md.unused_release_description,
            user=md.fw.user,
        )
    )
    fsck.add_fail(
        "Database::ReleaseDescription",
        "Repaired release descriptions from firmware #{}".format(md.fw.firmware_id),
    )


def _fsck_component_shards(fsck: Fsck, md: Component) -> None:

    repaired: int = 0
    removed: int = 0

    for shard in (
        db.session.query(ComponentShard)
        .filter(ComponentShard.component_id == md.component_id)
        .filter(
            or_(
                ComponentShard.plugin_id == None,
                ComponentShard.plugin_id == "",
                ComponentShard.plugin_id == "chipsec",
            )
        )
    ):
        shards_new = (
            db.session.query(ComponentShard)
            .filter(ComponentShard.component_id == md.component_id)
            .filter(ComponentShard.plugin_id != None)
            .join(ComponentShardChecksum)
            .filter(ComponentShardChecksum.value == shard.checksum)
            .limit(1)
            .all()
        )
        if shards_new:
            path = shard.absolute_path
            if os.path.exists(path):
                os.remove(path)
            db.session.delete(shard)
            removed += 1
        else:
            shard.plugin_id = "uefi-extract"
            repaired += 1
    if repaired:
        fsck.add_fail(
            "Database::Shards",
            "Repaired {} shards from firmware #{}".format(repaired, md.fw.firmware_id),
        )
    if removed:
        fsck.add_fail(
            "Database::Shards",
            "Removed {} shards from firmware #{}".format(removed, md.fw.firmware_id),
        )


def _get_yara_title(query: YaraQuery) -> Optional[str]:
    for line in query.value.replace("{", "\n").split("\n"):
        if line.startswith("rule "):
            return line[5:]
    return None


def _fsck_yara_query_fixup_title(fsck: Fsck, query: YaraQuery) -> None:
    """fix up user subgroups"""

    # already set
    if query.title or query.kind:
        return
    query.kind = "yara"
    query.title = _get_yara_title(query)
    fsck.add_fail(
        "Database::Query",
        "Set Yara title of {}".format(query.title),
    )
    db.session.commit()


def _fsck_user_fixup_subgroup(fsck: Fsck, user: User) -> None:
    """fix up user subgroups"""

    # already set
    if user.subgroup:
        return

    open_idx: int = user.display_name.find("(")
    close_idx: int = user.display_name.find(")")
    if open_idx == -1 or open_idx == -1 or open_idx > close_idx:
        return

    user.subgroup = user.display_name[open_idx + 1 : close_idx]
    user.display_name = user.display_name[:open_idx].strip()
    fsck.add_fail(
        "Database::Users",
        "Set subgroup of {} for user {}".format(user.subgroup, user.user_id),
    )
    db.session.commit()


def _fsck_user_fixup_disabled(fsck: Fsck, user: User) -> None:
    """fix up the disabled user details from the event log"""

    if user.auth_type != "disabled":
        return
    if not user.username.startswith("disabled_user"):
        return

    # find what we recorded
    evt = (
        db.session.query(Event)
        .filter(Event.message.startswith("Disabling user {} ".format(user.user_id)))
        .first()
    )
    if not evt:
        return

    # parse the string
    try:
        user.username = evt.message.split(" ")[3]
    except IndexError:
        return
    lbr = evt.message.find("(")
    rbr = evt.message.find(")")
    if lbr != -1 and rbr != -1 and rbr > lbr:
        user.display_name = evt.message[lbr + 1 : rbr]

    # record success
    fsck.add_fail(
        "Database::Users",
        "Restored username {} for user {}".format(user.username, user.user_id),
    )
    db.session.commit()


def _fsck_report_check_deprecated_attrs(fsck: Fsck, report: Report) -> None:
    """fix up report attrs"""

    fixed: List[str] = []
    for key, value in REPORT_ATTR_MAP.items():
        attr = report.get_attribute_by_key(key)
        if attr:
            attr.key = value
            if key not in fixed:
                fixed.append(key)
    if fixed:
        fsck.add_fail(
            "Database::Reports",
            "Fixed up {} keys".format(", ".join(fixed)),
        )
    db.session.commit()


@tq.task(max_retries=3, default_retry_delay=5, task_time_limit=18000)
def _async_fsck(extended: bool) -> None:

    fsck = Fsck()
    fsck.extended = extended
    fsck.container_id = os.environ.get("CONTAINER_ID")
    db.session.add(fsck)
    db.session.commit()

    # force user actions
    _user_disable_notify()
    _user_disable_actual()
    _user_add_message_survey()

    # these need a cache
    _fsck_firmware_unsigned(fsck)

    # fix up queries
    for (yara_query_id,) in db.session.query(YaraQuery.yara_query_id).order_by(
        YaraQuery.yara_query_id.asc()
    ):
        query = (
            db.session.query(YaraQuery)
            .filter(YaraQuery.yara_query_id == yara_query_id)
            .one()
        )
        _fsck_yara_query_fixup_title(fsck, query)

    # fixup certificate serial
    for crt in db.session.query(UserCertificate).order_by(
        UserCertificate.certificate_id.asc()
    ):
        if len(crt.serial) < 40:
            crt.serial = crt.serial.rjust(40, "0")
            fsck.add_fail(
                "Database::UserCertificate",
                "Repaired serial to {} for user {}".format(crt.serial, crt.user_id),
            )
            db.session.commit()

    # fix up users
    for (user_id,) in db.session.query(User.user_id).order_by(User.user_id.asc()):
        user = db.session.query(User).filter(User.user_id == user_id).one()
        _fsck_user_fixup_subgroup(fsck, user)
        _fsck_user_fixup_disabled(fsck, user)

    # fix up reports
    for (report_id,) in db.session.query(Report.report_id).order_by(
        Report.report_id.asc()
    ):
        report = db.session.query(Report).filter(Report.report_id == report_id).one()
        _fsck_report_check_deprecated_attrs(fsck, report)

    # process each task that does not require cache
    for (firmware_id,) in db.session.query(Firmware.firmware_id).order_by(
        Firmware.firmware_id.asc()
    ):
        if fsck.ended_ts:
            break
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
        _fsck_firmware_check_exists(fsck, fw)
        _fsck_firmware_consistency(fsck, fw)
        _fsck_firmware_resign(fsck, fw)
        _fsck_firmware_revisions(fsck, fw)
        _fsck_firmware_revision_basename(fsck, fw)
        if extended:
            _fsck_firmware_check_archive(fsck, fw)
        for md in fw.mds:
            _fsck_component_shards(fsck, md)
            _fsck_component_release_description(fsck, md)
            _fsck_component_translations(fsck, md)
            for issue in md.issues:
                _fsck_issue_upgrade(fsck, issue)
                _fsck_issue_value_fixup(fsck, issue)
            for kw in md.keywords:
                _fsck_keyword_value_fixup(fsck, kw)
        db.session.commit()

    # lets do the really slow ones last
    for (firmware_id,) in db.session.query(Firmware.firmware_id).order_by(
        Firmware.firmware_id.asc()
    ):
        if fsck.ended_ts:
            break
        _fsck_firmware_cve_descriptions(fsck, fw)

    # all done
    fsck.ended_ts = datetime.datetime.utcnow()
    db.session.commit()
