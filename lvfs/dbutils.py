#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-many-locals,too-many-statements,too-few-public-methods,protected-access

import gzip
import hashlib
import os
import random
import uuid
from typing import Dict, List, Any


def _make_boring(val: str) -> str:
    out = ""
    if not val:
        return out
    for v in val.lower():
        if "a" <= v <= "z":
            out += v
        elif v == " " and not out.endswith("_"):
            out += "_"
    for suffix in [
        "_company",
        "_corporation",
        "_enterprises",
        "_incorporated",
        "_industries",
        "_international",
        "_limited",
        "_services",
        "_studios",
        "_inc",
    ]:
        out = out.replace(suffix, "")
    return out


def _make_fake_ip_address() -> str:
    return "%i.%i.%i.%i" % (
        random.randint(1, 254),
        random.randint(1, 254),
        random.randint(1, 254),
        random.randint(1, 254),
    )


def _make_fake_version() -> str:
    return "%i.%i.%i" % (
        random.randint(0, 1),
        random.randint(1, 16),
        random.randint(1, 254),
    )


def anonymize_db(db: Any) -> None:
    from .vendors.models import Vendor
    from .firmware.models import Firmware

    # get vendor display names
    vendor_names: List[str] = []
    with gzip.open("data/vendors.txt.gz", "rb") as f:
        for ln in f.read().decode().split("\n"):
            if not ln:
                continue
            vendor_names.append(ln)
    random.shuffle(vendor_names)

    # get some plausible user names
    user_names: List[str] = []
    with gzip.open("data/users.txt.gz", "rb") as f:
        for ln in f.read().decode().split("\n"):
            if not ln:
                continue
            user_names.append(ln)
    random.shuffle(user_names)

    # get some plausible device names
    device_names: List[str] = []
    with gzip.open("data/devices.txt.gz", "rb") as f:
        for ln in f.read().decode().split("\n"):
            if not ln:
                continue
            device_names.append(ln)
    random.shuffle(device_names)

    # get some random words for keywords
    generic_words: List[str] = []
    with open("/usr/share/dict/words", "rb") as f2:
        for ln in f2.read().decode().split("\n"):
            if not ln:
                continue
            generic_words.append(ln)
    random.shuffle(generic_words)

    # anonymize vendors
    idx_generic_words = 0
    idx_user_names = 0
    idx_vendor_names = 0
    for v in db.session.query(Vendor):
        if not v.should_anonymize:
            continue
        v.display_name = vendor_names[idx_vendor_names]
        v.group_id = _make_boring(v.display_name)
        v.comments = "We pass no judgment"
        v.icon = "vendor-1.png"
        v.keywords = generic_words[idx_generic_words]
        v.oauth_unknown_user = None
        v.subgroups = None
        v.oauth_domain_glob = None
        v.username_glob = "*@" + v.group_id.replace("_", "") + ".com"
        v.remote.name = "embargo-" + v.group_id
        idx_generic_words += 1

        # anonymize restrictions
        for r in v.restrictions:
            r.value = "USB:0x0123"

        # anonymize users
        for u in v.users:
            if u.username == "sign-test@fwupd.org":
                continue
            u.display_name = user_names[idx_user_names]
            u.username = _make_boring(u.display_name) + u.vendor.username_glob[1:]
            u.subgroup = _make_boring(u.subgroup)
            idx_user_names += 1
            for crt in u.certificates:
                crt.serial = "deadbeefdeadbeefdeadbeefdeadbeefdeadbeef"
                crt.text = (
                    "-----BEGIN CERTIFICATE-----\nFUBAR\n-----END CERTIFICATE-----"
                )
        idx_vendor_names += 1

    # anonymize firmware
    idx_device_names = 0
    device_names_existing: Dict[str, str] = {}
    for fw in db.session.query(Firmware):
        if not fw.vendor.should_anonymize:
            continue
        for md in fw.mds:
            md.checksum_contents_sha1 = hashlib.sha1(os.urandom(32)).hexdigest()
            md.checksum_contents_sha256 = hashlib.sha256(os.urandom(32)).hexdigest()
            for csum in md.device_checksums:
                csum.kind = "SHA1"
                csum.value = hashlib.sha1(os.urandom(32)).hexdigest()
            if md.name not in device_names_existing:
                device_names_existing[md.name] = device_names[idx_device_names]
                idx_device_names += 1
            md.name = device_names_existing[md.name]
            md.summary = "Firmware for the " + md.name
            md.description = None
            md.source_url = None
            md.unused_release_description = None
            for tx in md.release_descriptions:
                tx.value = "This fixes some bugs"
            md.url_homepage = "https://www." + fw.vendor.username_glob[2:]
            md.details_url = "https://www." + fw.vendor.username_glob[2:]
            md.filename_contents = "firmware.bin"
            md.release_timestamp = 0
            md.version = _make_fake_version()
            md.release_installed_size = random.randint(100000, 1000000)
            md.release_download_size = random.randint(200000, 1000000)
            md.screenshot_url = None
            md.screenshot_url_safe = None
            md.screenshot_caption = None
            md.release_message = None
            md.release_image = None
            md.release_image_safe = None
            md.appstream_id = (
                "com." + fw.vendor.group_id + "." + _make_boring(md.name) + ".firmware"
            )
            for gu in md.guids:
                gu.value = str(uuid.uuid4())
            for kw in md.keywords:
                kw.value = generic_words[idx_generic_words]
                idx_generic_words += 1

        # components now changed
        fw.addr = _make_fake_ip_address()
        fw.checksum_upload_sha1 = hashlib.sha1(os.urandom(4096)).hexdigest()
        fw.checksum_upload_sha256 = hashlib.sha256(os.urandom(4096)).hexdigest()
        fw.checksum_signed_sha1 = hashlib.sha1(os.urandom(4096)).hexdigest()
        fw.checksum_signed_sha256 = hashlib.sha256(os.urandom(4096)).hexdigest()
        fw._filename = None
        for rev in fw.revisions:
            rev.filename = (
                fw.checksum_upload_sha256
                + "-"
                + fw.vendor.group_id
                + "-"
                + _make_boring(fw.md_prio.name)
                + "-"
                + fw.version_display
                + ".cab"
            )

    # phew!
    db.session.commit()


def init_db(db: Any) -> None:

    # ensure all tables exist
    db.metadata.create_all(bind=db.engine)

    # ensure admin user exists
    from .vendors.models import Vendor
    from .verfmts.models import Verfmt
    from .protocols.models import Protocol
    from .categories.models import Category
    from .licenses.models import License
    from .users.models import User, UserAction
    from .metadata.models import Remote
    from .hash import _otp_hash

    if not db.session.query(Remote).filter(Remote.name == "stable").first():
        db.session.add(Remote(name="stable", is_public=True))
        db.session.add(Remote(name="testing", is_public=True))
        db.session.add(Remote(name="private"))
        db.session.add(Remote(name="deleted"))
        db.session.commit()
    if not db.session.query(Verfmt).filter(Verfmt.value == "triplet").first():
        db.session.add(Verfmt(value="quad", name="Quad"))
        db.session.add(Verfmt(value="triplet", name="Triplet"))
        db.session.add(Verfmt(value="pair", name="Pair"))
        db.session.add(Verfmt(value="plain", name="Plain"))
        db.session.commit()
    if (
        not db.session.query(Protocol)
        .filter(Protocol.value == "com.hughski.colorhug")
        .first()
    ):
        db.session.add(
            Protocol(value="com.hughski.colorhug", name="ColorHug", is_public=True)
        )
        db.session.add(Protocol(value="org.usb.dfu", name="USB DFU", is_public=True))
        db.session.add(
            Protocol(value="org.uefi.capsule", name="UEFI Capsule", is_public=True)
        )
        db.session.add(
            Protocol(value="org.dmtf.redfish", name="Redfish", is_public=True)
        )
        db.session.commit()
    if not db.session.query(Category).filter(Category.value == "triplet").first():
        db.session.add(Category(value="X-Device", name="Device Update"))
        db.session.add(Category(value="X-ManagementEngine", name="ME Update"))
        db.session.commit()
    if not db.session.query(License).filter(License.value == "CC0-1.0").first():
        db.session.add(License(value="CC0-1.0", is_content=True))
        db.session.add(License(value="GPL-2.0+", requires_source=True))
        db.session.add(License(value="LicenseRef-proprietary"))
        db.session.commit()
    if (
        not db.session.query(User)
        .filter(User.username == "sign-test@fwupd.org")
        .first()
    ):
        remote = Remote(name="embargo-admin")
        remote.access_token = "admin"
        db.session.add(remote)
        db.session.commit()
        vendor = Vendor(group_id="admin")
        vendor.display_name = "Acme"
        vendor.legal_name = "Acme Corp."
        vendor.psirt_url = "https://acme.com/psirt"
        vendor.missing_url = "https://acme.com/missing"
        vendor.subgroups = "dummy"
        vendor.remote_id = remote.remote_id
        db.session.add(vendor)
        db.session.commit()
        u = User(
            username="sign-test@fwupd.org",
            auth_type="local",
            otp_secret=_otp_hash(),
            display_name="Admin User",
            vendor_id=vendor.vendor_id,
        )
        u.actions.append(UserAction(value="admin"))
        u.actions.append(UserAction(value="qa"))
        u.actions.append(UserAction(value="analyst"))
        u.actions.append(UserAction(value="notify-server-error"))
        u.password = "Pa$$w0rd"
        db.session.add(u)
        db.session.commit()
    if not db.session.query(User).filter(User.username == "anon@fwupd.org").first():
        db.session.add(
            User(username="anon@fwupd.org", display_name="Anonymous User", vendor_id=1)
        )
        db.session.commit()


def drop_db(db: Any) -> None:
    db.metadata.drop_all(bind=db.engine)
